# Create a Snap (some personal notes)

## snapcraft.yaml

### lifecycle

[lifecycle](https://snapcraft.io/docs/parts-lifecycle)

1.  **pull**: downloads or otherwise retrieves the components needed to build the part. You can use the [`source-*` keywords](https://snapcraft.io/docs/snapcraft-parts-metadata#heading--source) of a part to specify which components to retrieve. If `source` points to a git repository, for example, the pull step will clone that repository.
2.  **build**: constructs the part from the previously pulled components. The [`plugin`](https://snapcraft.io/docs/snapcraft-plugins) of a part specifies how it is constructed. The [`meson` plugin](https://snapcraft.io/docs/meson-plugin), for example, executes `meson` and `ninja` to compile source code. Each part is built in a separate directory, but it can use the contents of the staging area if it specifies a dependency on other parts using the `after` keyword. See [Step dependencies](https://snapcraft.io/docs/parts-lifecycle#heading--step-dependencies) for more information.
3.  **stage**: copies the built components into the staging area. This is the first time all the different parts that make up the snap are actually placed in the same directory. If multiple parts provide the same file with differing contents, you will get a conflict. You can avoid these conflicts by using the [`stage` keyword](https://snapcraft.io/docs/snapcraft-parts-metadata#heading--stage) to white- or black-list files coming from the part. You can also use this keyword to filter out files that are not required in the snap itself, for example build files specific to a single part.
4.  **prime**: copies the staged components into the priming area, to their final locations for the resulting snap. This is very similar to the stage step, but files go into the priming area instead of the staging area. The `prime` step exists because the staging area might still contain files that are required for the build but not for the snap. For example, if you have a part that downloads and installs a compiler, then you stage this part so other parts can use the compiler during building. You can then use the `prime` filter keyword to make sure that it doesn’t get copied to the priming area, so it’s not taking up space in the snap. Some extra checks are also run during this step to ensure that all dependencies are satisfied for a proper run time. If confinement was set to `classic`, then files will be scanned and, if needed, patched to work with this confinement mode.

## Install snapcraft

```
sudo snap install snapcraft --classic
```

## Init Snap-Package

```bash
snapcraft init
```

## Build Snap

```bash
snapcraft --debug
snapcraft prime --shell --debug # befor step prime
snapcraft prime --shell-after --debug # after step prime

snapcraft <step> <part>
```

## Remove

### All

```bash
snapcraft clean
```

### Specific step of all parts

```bash
snapcraft clean -s build
```

### Specific step of a specific part

```bash
snapcraft clean <part> -s <step>
```

## Install Local

```bash
sudo snap install my-snap-name_0.1_amd64.snap --dangerous --devmode
```

## use setup.py

[link](https://snapcraft.io/docs/using-external-metadata)

```yaml
name: sampleapp-name
summary: sampleapp summary
adopt-info: sampleapp

apps:
  sampleapp:
    command: sampleapp

parts:
  sampleapp:
    plugin: python
    source: http://github.com/example/sampleapp.git
    parse-info: [setup.py]
```
