README FOR CODE_ASTER INSTALLATION
==================================

.. This file is also available in html format. Open README_aster.html in favorite browser.

If you want to install a qualified version for EDF studies, you must download salome_meca.


.. contents:: Summary


For further informations visit http://www.code-aster.org.
Please report installation problems using *Installation* forum
at http://www.code-aster.org.

Read carefully this README or at least help message given by ::

   python setup.py --help


Prerequisites
-------------

code_aster requires Python >= 3.5.

You must install the python-dev or python-devel package (the exact package name
depends on your distribution) or compile it from sources.
Python and Numpy header files are required.
Some other libraries are required (mathematical blas/lapack libs).

- Example of the additional prerequisites needed on Ubuntu 18.04:

    - bison
    - make
    - cmake
    - flex
    - g++
    - gcc
    - gfortran
    - grace
    - libopenblas-dev
    - python
    - python-dev
    - python-numpy
    - tk
    - zlib1g-dev

- Example of the additional prerequisites needed on CentOS 8:
    - bison
    - make
    - cmake
    - flex
    - gcc
    - gcc-c++
    - gcc-gfortran
    - python3-devel
    - python3-numpy
    - python3-sphinx
    - lapack-devel
    - ImageMagick
    - graphviz
    - tk
    - zlib-devel


- Note about compilers

  ``setup.py`` tries now to find automatically your compilers.
  By default it searches GNU compilers.
  After searching it writes found values. Check them before continue.
  You can always set them using the ``setup.cfg`` configuration file.
  You can also use ``PATH`` and ``LD_LIBRARY_PATH`` environment variables
  to help ``setup.py`` to find the compiler you prefer.


License and packaging
---------------------

code_aster is distributed under the terms of the GNU General Public License.
The text is available at : http://code-aster.org/spip.php?article306

IMPORTANT NOTICE:
    code_aster is a free software but not aster-full package !
    The aster-full archive does not contain only open-source softwares :
    homard is not open-source but it can be freely distributed
    with code_aster.

Why this kind of package (the big archive aster-full) ?
Why "apt-get install codeaster" does not work (yet) ?
Some answers at http://www.code-aster.org/forum2/viewtopic.php?id=13292


For really impatient
--------------------

If you have a recent Linux distribution you may try directly ::

      python setup.py install --prefix=$HOME/aster

The setup script needs Python >=3.5 with numpy.

Jump to FULL INSTALLATION step for more details.


Distribution
------------

The package "aster-xxx.tar" contains :

    - ``setup.py`` (and others ``.py``) : setup scripts
    - ``setup.cfg`` : configuration file to adjust according to your configuration.
    - ``SRC/`` : archives of code_aster and its prerequisites :

        - code_aster 14.6.0,
        - astk 2020.0 (code_aster study manager),
        - mumps 5.1.2 (direct solver library),
        - mfront 3.2.1 (toolkit for mechanical behaviors),
        - med 4.0.0 (Data Exchange Model),
        - hdf5 1.10.3 (platform independent file format),
        - metis 5.1.0 (official release),
        - scotch 6.0.4_esmumps (partitioning tool),
        - homard 11.12 (refine/unrefine meshes).        [binary]


Important note :

    homard is provided here as precompiled for Linux.
    When installing these products on another platform type they
    must be disabled (see INSTALLATION WITH PRODUCTS SELECTION section).
    Binaries for some other platforms may be available from
    http://www.code-aster.org, or directly from their editor.


Preferences
-----------

The setup will automatically search for basic components in
standard directories (in this order) ::

    /usr/local/bin, /usr/bin, /bin, /usr/X11R6/bin,
    /usr/bin/X11 (Tru64 specific),
    /usr/openwin/bin (Solaris specific).

(analog paths are used to search for libraries and includes)

You can set ``BINDIR`` (or ``LIBDIR``, ``INCLUDEDIR``) to force the script
to search also in your own directories. Edit ``setup.cfg`` and
for example ::

    BINDIR=['/home/aster/public/bin', '/opt/bin']

Note :

    Following common tools are automatically searched during the setup :
    shell interpreter (one choosen between bash, ksh or zsh),
    terminal (on choosen between xterm -recommended-, gnome-terminal, konsole),
    text editor (one choosen between gedit, kwrite, xemacs, emacs,
    xedit or vi).

Note for developpers :

    If you want to install a code_aster version to make development, following
    these steps:

    - install this "testing" version and its prerequisites::

        ASTER_ROOT=$HOME/aster
        python setup.py install --prefix=$ASTER_ROOT

    - clone the source repository and read the Tutorial on Bitbucket
      (see https://sourceforge.net/p/codeaster/wiki/Tutorial)::

        mkdir -p $HOME/dev/codeaster && cd $HOME/dev/codeaster
        hg clone http://hg.code.sf.net/p/codeaster/src src

    - configure the build using the prerequisites generated by ``setup.py``::

        cd $HOME/dev/codeaster/src
        ./waf configure --use-config-dir=$ASTER_ROOT/14.6/share/aster \
                        --use-config=aster_full_config --prefix=../install/std
        ./waf install -p

    - continue with the Tutorial to add your development version in astk...


Full installation
-----------------

Using this mode you wish install all products included in the package.

Note :
    Read carefully setup.cfg comments to customize your installation.

1. Complete ``setup.cfg`` file to adjust it regarding your
   configuration. Only ``ASTER_ROOT`` is necessary.
   Read carefully comments describing the optional parameters.

   Note :
      you can also use ``--prefix=$ASTER_ROOT`` option of setup.py to
      change ``ASTER_ROOT`` destination directory (equivalent to ``--aster_root``).

2. Run setup script (with the right Python interpreter) ::

      python setup.py install [--prefix=...]

   The built of code_aster takes about 10-15 min and the built of the
   prerequisites takes about 15-20 min. The real time depends on your
   configuration.

   Note :
      ``python setup.py --help`` prints informations about available options
      and products selection.


Installation with products selection
------------------------------------

You have two possibilities to select which products you will
install : giving the list of products to install (see 1.)
or specify the products you don't want to install (see 2.).

1. You can give the name of the products to install (space separated) ::

        python setup.py install --prefix=... product1 [products2 [...]]


2. You can disable a product installation by adding lines to
   ``setup.cfg`` ::

        _install_PRODUCT_NAME = False


If an error occurs in a prerequisite
------------------------------------

Error during installation of a product (for example : the
compilation of med failed).
There isn't yet a way to customize configuration or built of a
product through ``setup.cfg`` option.
So you have to configure, build and install the product by hand
(source archive is in ``SRC`` directory), and set variables which
are normally set by product setup by adding lines into
``setup.cfg`` file.
It isn't always simple to know what variables must be set...
so ask the forum on www.code-aster.org !
When the product is installed add a ``_install_XXX=False`` line
to ``setup.cfg`` (see *EXAMPLES* section).


If an error occurs in code_aster installation
---------------------------------------------

- Enter in the working directory (see ``setup.log`` file, for example:
  ``/tmp/install_aster.12345/aster-14.6.0``).

- Set the build environment::

    source env.d/aster_full_std.sh

- And try to replay the ``waf`` commands marked as **FAILED** (*Configuration*,
  *Building the product* or *Installation*.


Log output
----------

If an error occurs it's better to attach all the traceback if
you ask any questions to code_aster forum.
All output informations are saved into the 'setup.log.*' files.
Save all output informations into an archive ::

    tar cvjf install_problem.tar.bz2 setup.log.* setup.dbg.*

Warning: the size limit of attachments is 2 MB.


Note about Intel Compilers
--------------------------

Intel compilers can be used for aster, mumps and metis for better
performances but it may make the build more complicated.
Its is recommended to keep GNU compilers at least for the other products.
To do that just edit ``setup.cfg`` and uncomment these lines ::

    PREFER_COMPILER_aster = 'Intel'
    PREFER_COMPILER_mumps = PREFER_COMPILER_aster
    PREFER_COMPILER_metis = PREFER_COMPILER_aster

You can "source" the compiler environment script, ``iccvars.sh``,
``ifortvars.sh`` and ``mklvarsXX.sh`` to set environment variables
for Intel Compilers to help ``setup.py`` to find your compilers.
For example ::

    source /opt/intel/cc/*/bin/iccvars.sh
    source /opt/intel/fc/*/bin/ifortvars.sh
    source /opt/intel/mkl/*/tools/environment/mklvars32.sh
    python setup.py install --prefix=$HOME/aster
    ...


Test your installation
----------------------

Following steps suppose installation (python setup.py install...)
ended with a correct diagnostic ::

    --- DIAGNOSTIC JOB : OK


1. Run a simple test case ::

      $HOME/aster/bin/as_run --vers=14.6 --test forma01a


2. Run a list of test cases

   - Change to your ``ASTER_ROOT`` directory and enter in
     ``ASTER_VERSION`` directory ::

      cd $HOME/aster/14.6

   - Build the list of testcases to run::

      $HOME/aster/bin/as_run --list --all --vers=14.6 \
            --filter='"parallel" not in testlist' \
            -o /tmp/list_seq

     For a parallel installation, use the filter: ``--filter='"parallel" in testlist'``

   - Run the testlist from astk or create an export file::

        cd $HOME/aster/14.6/share/aster

        cat << EOF > astout.export
        # parameters
        P actions astout
        P version 14.6
        P debug nodebug
        P mode interactif
        P ncpus 1
        P nbmaxnook 500
        P cpresok RESOK
        P facmtps 3
        P tpsjob 300

        # list of the test to run
        F list /tmp/list_seq D 0

        # results destination
        R resu_test /tmp/resu_test R 0
        EOF

   - Start the complete test suite ::

      $HOME/aster/bin/as_run astout.export

   Note :
      By default ``astout.export`` takes the list of testcases from
      ``/tmp/list_seq`` (about 3000 testcases).
      Just edit it to run less testcases.
