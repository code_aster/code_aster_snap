
# -*- coding: utf-8 -*-
# COPYRIGHT (C) 1991 - 2020  EDF R&D                  WWW.CODE-ASTER.ORG
"Package info"

modname      = 'codeaster-setup'
version      = '14.6.0'
numversion   = (14, 6, 0)
release      = '1'

license      = 'GPL, LGPL, non-free'
copyright    = 'Copyright (c) 2001-2020 EDF R&D - http://www.code-aster.org'

short_desc   = "Setup script for code_aster and some prerequisites"
long_desc    = short_desc

author       = "EDF R&D"
author_email = "code-aster@edf.fr"

dict_prod = {'aster': '14.6.0',
 'astk': '2020.0',
 'hdf5': '1.10.3',
 'homard': '11.12',
 'med': '4.0.0',
 'metis': '5.1.0',
 'mumps': '5.1.2',
 'scotch': '6.0.4',
 'tfel': '3.2.1'}

dict_prod_param = {'__to_install__': ['hdf5',
                    'med',
                    'homard',
                    'scotch',
                    'aster',
                    'metis',
                    'mumps',
                    'astk',
                    'tfel'],
 'aster-verslabel': 'stable'}

